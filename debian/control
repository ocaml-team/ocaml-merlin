Source: ocaml-merlin
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders: Julien Puydt <jpuydt@debian.org>
Section: ocaml
Priority: optional
Standards-Version: 4.7.0
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-ocaml, dh-vim-addon,
               libalcotest-ocaml-dev,
               libcsexp-ocaml-dev,
               libfindlib-ocaml-dev,
               libmenhir-ocaml-dev,
               libyojson-ocaml-dev,
               menhir,
               ocaml-dune,
               ocaml-findlib,
               ocaml
Vcs-Browser: https://salsa.debian.org/ocaml-team/ocaml-merlin
Vcs-Git: https://salsa.debian.org/ocaml-team/ocaml-merlin.git
Homepage: https://ocaml.github.io/merlin

Package: ocaml-merlin
Architecture: any
Depends: ${misc:Depends}, ${ocaml:Depends}, ${shlibs:Depends}
Provides: ${ocaml:Provides}
Recommends: ocaml-findlib
Description: editor service to develop in OCaml (common files)
 This package provides an editor service for advanced
 integrated development environment (IDE) in the OCaml
 language both for the vim and emacs editors.
 .
 This is the common files, with vim-ocaml-merlin and
 emacs-ocaml-merlin providing the actual support for your
 favorite editor.

Package: vim-ocaml-merlin
Architecture: any
Depends: ocaml-merlin, vim-python3, ${misc:Depends}, ${vim-addon:Depends}
Description: editor service to develop in OCaml (vim files)
 This package provides an editor service for advanced
 integrated development environment (IDE) in the OCaml
 language both for the vim and emacs editors.
 .
 This is the vim-specific package.

Package: emacs-ocaml-merlin
Architecture: any
Depends: ocaml-merlin, ${misc:Depends}
Description: editor service to develop in OCaml (emacs files)
 This package provides an editor service for advanced
 integrated development environment (IDE) in the OCaml
 language both for the vim and emacs editors.
 .
 This is the emacs-specific package.

